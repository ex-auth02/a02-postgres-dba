# Inicializacion 
- Cree un droplet o ec2 con docker instalado 
- enviar el contenido a la Cloud Vm via rsyncs 
```sh 
rsync -az --progress --exclude={'node0/*','.git'} -r $(pwd)/ root@ip-addr:/usr/src/
```
- navegue a cd /usr/src/ 

## dentro de ese directorio ejecute
```sh 
$ docker-compose up -d 
```
###### automaticamente se creara un contenedor con un volumen enlazado
###### Acceso
existen varias maneras de acceso pero dejo dos maneras automatizadas 
- conectandose a la consola del contenedor 

```sh 
$ docker exec -it $(docker ps -aq) /bin/bash 
```
- conectandose directamente a la consola de psql de postgres 
```sh 
$ docker exec -it $(docker ps -aq) psql -U postgres
```

