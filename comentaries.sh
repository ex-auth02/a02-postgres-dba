docker rm -f postgre-ex1 && docker volume rm postgre-p0

#resetear el id pk de alguna tabla 
#\ds consulta los los atributos seq 4
ALTER SEQUENCE tipo_usuarios(id_tipo_usr) RESTART WITH 1;

docker exec -it postgre-ex1 psql -f dml_prototype.sql -U postgres a02_test 


#termina todas las seciones del servidor
SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'a02_test' 
  AND pid <> pg_backend_pid();


