\c a02_test;

INSERT INTO tipo_usuarios(privilegio,tipo_usuario)VALUES
    ('ADMIN', 'A01x'),
    ('PLATA', 'A02x'),
    ('BRONCE', 'A03x'),
    ('it-epsilondelta', '00xFF');

INSERT INTO tipo_transportes(tipo_transporte)VALUES 
    ('Bicicleta'),
    ('Bicicleta Electrica'),
    ('Moto'),
    ('Moto Electrica'), 
    ('Auto liviano'), 
    ('Camioneta'), 
    ('Camion'),    
    ('Furgon');    

INSERT INTO  tipo_telefonos(tipo_telefono) VALUES
    ('Celular'), 
    ('Fijo'),
    ('VoIP'),
    ('Satelital');

INSERT INTO usuarios(status, email, passwd, dni, profecion, apellido, nombre, id_tipousr) VALUES
    (TRUE, 'auxx2@etx.com', 'asfdgfhg', '21312314', 'Eth sec', 'ether', 'xas', 1),
    (TRUE, 'auvsi@etx.com', 'asdfkadsf', '21312343', 'Backend', 'start', 'desc', 1),
    (FALSE, 'mafd@darpa.com', 'wqejlkrfdfsk', '21312312', 'frontend', 'iquix', 'mixta', 1),
    (TRUE, 'asdas@darpa.com', 'alphatauri', '21312323', 'ux', 'miachael', 'strada', 2),
    (TRUE, 'asdsd@hexa.com', 'wqejrk@$#@d', '32312311', 'PM', 'Gorco', 'Dario', 2),
    (FALSE, 'afr3@hexa.com', 'wqasdas#$@#', '84244686', 'UI', 'Denet', 'ethx', 3);

INSERT INTO transportes(habilitado, marca, foto_vehiculo, 
            color, id_usuario, id_tipo_trs, id_patente)VALUES
    (TRUE, 'Aventon', '/vol/codx_f_usr.png', 'black', 1, 1, 'EX01X'),
    (TRUE, 'CANYON', '/vol/cod1_f_usr.png', 'whitte', 1, 1, 'EX02X'),
    (FALSE, 'CANYON', '/vol/cod2_f_usr.png', 'whitte', 2, 2, 'EX03X'),
    (TRUE, 'FIAT', '/vol/cod04_f_usr.png', 'BRONW', 4, 5, 'EXF2X'),
    (TRUE, 'MERCEDEZ', '/vol/cod1_f_usr.png', 'whitte', 3, 7, 'EX023X'),
    (TRUE, 'Toyota', '/vol/cod1_f_usr.png', 'BLUE', 6, 6, 'UX02X'); 

INSERT INTO telefono_usuarios(telefono, id_usuario, id_tipo_tel)VALUES
    (1131332, 3, 1),
    (1231236, 2, 3), 
    (2131235, 5, 4), 
    (3453227, 1, 2);
    