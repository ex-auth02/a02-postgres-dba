CREATE DATABASE a02_test;
\c a02_test;

CREATE TABLE regiones(
    nombre_region VARCHAR(60),
    vector VARCHAR(60),
    id_region SERIAL NOT NULL UNIQUE,
    CONSTRAINT region_pk PRIMARY KEY (id_region)
);

CREATE TABLE municipios(
    nombre_municipio VARCHAR(60), 
    vector VARCHAR(60), 
    id_municipio SERIAL NOT NULL UNIQUE,
    CONSTRAINT municipio_pk PRIMARY KEY (id_municipio)
); 

CREATE TABLE distritos_municipales(
    nombre_distrito VARCHAR(60),
    vector VARCHAR(60), 
    id_distrito SERIAL NOT NULL UNIQUE,
    CONSTRAINT distrito_municipal_pk PRIMARY KEY (id_distrito)
);

CREATE TABLE provincias(
    nombre_provincia VARCHAR(45),
    vector VARCHAR(45),
    id_region INT NOT NULL,
    id_provincia SERIAL NOT NULL UNIQUE, 
    CONSTRAINT provincia_pk PRIMARY KEY (id_provincia)
);




--Aplicacion de not null en los atrubutos de cada unos de los ids 
CREATE TABLE drm_municipios(  
    id_provincia INT,
    id_municipio INT, 
    id_distrito INT,
    id_drm_type SERIAL NOT NULL UNIQUE, 
    CONSTRAINT drm_type_pk PRIMARY KEY  (id_drm_type)
); 

CREATE TABLE tipo_usuarios(
    privilegio VARCHAR(64) NOT NULL, 
    tipo_usuario VARCHAR(32) NOT NULL, 
    id_tipo_usr SERIAL NOT NULL UNIQUE, 
    CONSTRAINT tipo_usr_pk PRIMARY KEY (id_tipo_usr)
);

CREATE TABLE  tipo_transportes(
    tipo_transporte VARCHAR(50),
    id_tipo SERIAL NOT NULL UNIQUE,
    CONSTRAINT tipo_trs_pk PRIMARY KEY (id_tipo)
);

CREATE TABLE tipo_telefonos(
    tipo_telefono VARCHAR(20),
    id_tipo_tel SERIAL NOT NULL UNIQUE,
    CONSTRAINT tipo_tel_pk PRIMARY key (id_tipo_tel)
);


--doble relacion

CREATE TABLE usuarios(
    status BOOLEAN, 
    email VARCHAR(160) UNIQUE, 
    passwd VARCHAR(200), 
    dni VARCHAR(9) UNIQUE, 
    profecion VARCHAR(80), 
    apellido VARCHAR(60),
    nombre VARCHAR(60),
    id_tipousr INT NOT NULL,
    id_usuario SERIAL NOT NULL UNIQUE,
    CONSTRAINT usr_pk PRIMARY KEY (id_usuario)
);

COMMENT ON TABLE usuarios IS 'se han eliminando algunos atributos de la tabla 
y las relaciones estan asiganadas al final del documento';


--tablas transitivas 

CREATE TABLE transportes(
    habilitado BOOLEAN, 
    marca VARCHAR(50),
    foto_vehiculo VARCHAR(50),
    color VARCHAR(50),  /* podria ser un rango de hexadecimanles para el openCV de una camara*/
    id_usuario INT,
    id_tipo_trs INT NOT NULL,
    id_patente VARCHAR(10) NOT NULL UNIQUE,
    CONSTRAINT patente_pk PRIMARY KEY (id_patente)
);



CREATE TABLE telefono_usuarios(
    telefono INT,
    id_usuario INTEGER NOT NULL,
    id_tipo_tel INTEGER NOT NULL, 
    id_telefono SERIAL NOT NULL UNIQUE,
    CONSTRAINT telefono_pk PRIMARY KEY (id_telefono)
);

--ONE RELACION 

ALTER TABLE usuarios ADD CONSTRAINT tipousr_fk 
FOREIGN KEY  (id_tipousr) REFERENCES tipo_usuarios(id_tipo_usr) 
ON DELETE NO ACTION
ON UPDATE CASCADE;

--TRANSITIVES 

ALTER TABLE transportes ADD CONSTRAINT tipo_trs_fk
FOREIGN KEY (id_tipo_trs) REFERENCES tipo_transportes(id_tipo)
ON DELETE NO ACTION
ON UPDATE CASCADE; 

ALTER TABLE transportes ADD CONSTRAINT tipo_usr_fk 
FOREIGN KEY (id_usuario) REFERENCES usuarios(id_usuario)
ON DELETE NO ACTION
ON UPDATE CASCADE;


ALTER TABLE telefono_usuarios ADD CONSTRAINT id_usuario_pk
FOREIGN KEY (id_usuario) REFERENCES usuarios(id_usuario)
ON DELETE NO ACTION 
ON UPDATE CASCADE;

ALTER TABLE telefono_usuarios ADD CONSTRAINT tipo_tel_pk
FOREIGN KEY (id_tipo_tel) REFERENCES tipo_telefonos(id_tipo_tel)
ON DELETE NO ACTION 
ON UPDATE CASCADE;


ALTER TABLE provincias ADD CONSTRAINT id_region_fk
FOREIGN KEY (id_region) REFERENCES regiones(id_region)
ON DELETE NO ACTION
ON UPDATE CASCADE; 


--TRANSITIVE LAYER OF MORE PK 

ALTER TABLE drm_municipios ADD CONSTRAINT id_distrito_fk
FOREIGN KEY  (id_distrito) REFERENCES  distritos_municipales(id_distrito)
ON DELETE NO ACTION
ON UPDATE CASCADE; 

ALTER TABLE drm_municipios ADD CONSTRAINT id_municipio_fk
FOREIGN KEY (id_municipio) REFERENCES municipios(id_municipio)
ON DELETE NO ACTION
ON UPDATE CASCADE;

ALTER TABLE drm_municipios ADD CONSTRAINT id_provincia_fk
FOREIGN KEY (id_provincia) REFERENCES provincias(id_provincia)
ON DELETE NO ACTION
ON UPDATE CASCADE; 